package speech.com.myapplication;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

import okhttp3.OkHttpClient;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class MainActivity extends Activity implements SendCommandTask.IOnPostExecute {

	private TextView txtSpeechInput;
	private ImageButton btnSpeak;
	private TextView resultTextView;
	private TextView ipText;
	private TextView portText;


	private final Locale LOCALE = Locale.forLanguageTag("ES");
	private final int REQ_CODE_SPEECH_INPUT = 100;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		/**
		 * Obtener referencias a las vistas de la interfaz
		 */
		txtSpeechInput = findViewById(R.id.txtSpeechInput);
		btnSpeak = findViewById(R.id.btnSpeak);
		resultTextView = findViewById(R.id.resultTextView);
		ipText = findViewById(R.id.ipField);
		portText = findViewById(R.id.portField);

		// hide the action bar
		getActionBar().hide();


		/**
		 * Establecer accion para el boton de reconocimiento
		 */
		btnSpeak.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				promptSpeechInput();
			}
		});
	}

	private void promptSpeechInput() {
		/**
		 * Establecer parametros necesarios para el reconocimiento de voz (lenguaje, tipo de mensaje, etc...)
		 */
		Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
				RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, LOCALE);
		intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
				getString(R.string.speech_prompt));
		try {
			/**
			 * Solicitar al sistema el reconocimiento
			 */
			startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
		} catch (ActivityNotFoundException a) {
			Toast.makeText(getApplicationContext(),
					getString(R.string.speech_not_supported),
					Toast.LENGTH_SHORT).show();
		}
	}

	/**
	 * Recibir el resultado del reconocmiento cuando se llama a startActivityForResult
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {
		case REQ_CODE_SPEECH_INPUT:
			{
				if (resultCode == RESULT_OK && null != data) {

					ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

					txtSpeechInput.setText(result.get(0));

					updateStatusLabel(R.string.reconocido);

					/**
					 * Extraer de la cadena de caracteres numeros (ver extractQueryForMsg())
					 */
					final SoundQuery query = extractQueryFromMsg(result.get(0));
					if (query != null) {
						/**
						 * Si obtenemos algo de la cadena, enviar al servidor el ID extraido
						 * este sera siempre un numero entero
						 */
						SendCommandTask task = new SendCommandTask(getDefaultSoundApiInstance(), query, this);
						task.execute();
					} else {
						resultTextView.setText(R.string.error);
					}
				}
				break;
			}
		}
	}

	private SoundQuery extractQueryFromMsg(String msg) {

		if (msg.contains("2") || msg.contains("dos")) {
			return new SoundQuery(2);
		} else if (msg.contains("3") || msg.contains("tres")) {
			return new SoundQuery(3);
		} else if (msg.contains("4") || msg.contains("cuatro")) {
			return new SoundQuery(4);
		} else if (msg.contains("5") || msg.contains("cinco")) {
			return new SoundQuery(5);
		} else if (msg.contains("6") || msg.contains("seis")) {
			return new SoundQuery(6);
		} else if (msg.contains("7") || msg.contains("siete")) {
			return new SoundQuery(7);
		} else if (msg.contains("8") || msg.contains("ocho")) {
			return new SoundQuery(8);
		} else if (msg.contains("9") || msg.contains("nueve")) {
			return new SoundQuery(9);
		} else if (msg.contains("10") || msg.contains("diez")) {
		 	return new SoundQuery(10);
		} else if (msg.contains("1") || msg.contains("uno")) {
			return new SoundQuery(1);
		} else if (msg.contains("0") || msg.contains("cero")) {
			return new SoundQuery(0);
		} else {
			return null;
		}
	}

	@Override
	public void onCommandSent(SoundQuery query, Response<Void> response) {
		if (response != null && response.isSuccessful())
			updateStatusLabel("Comando enviado id: " + query.getSoundID());
		else {
			updateStatusLabel(R.string.error);
		}
	}

	private SoundApi getDefaultSoundApiInstance() {
		String address = getIpAddress();
		String port = getPort();
		String baseUrl = String.format("http://%s:%s", address, port);

		Retrofit.Builder builder = new Retrofit.Builder();
		OkHttpClient.Builder okBuilder = new OkHttpClient.Builder();
		return builder
				.baseUrl(baseUrl)
				.addConverterFactory(JacksonConverterFactory.create())
				.client(okBuilder.build())
				.build()
				.create(SoundApi.class);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private String getIpAddress() {
		return ipText.getText().toString();
	}

	private String getPort() {
		return portText.getText().toString();
	}

	private void updateStatusLabel(int stringID) {
		resultTextView.setText(stringID);
	}

	private void updateStatusLabel(String msg) {
		resultTextView.setText(msg);
	}

}


