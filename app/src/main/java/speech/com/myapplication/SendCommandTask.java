package speech.com.myapplication;

import android.os.AsyncTask;
import java.io.IOException;
import java.lang.ref.WeakReference;

import retrofit2.Response;

public class SendCommandTask extends AsyncTask<SoundQuery, Void, Response<Void>> {

    private final WeakReference<SoundApi> mService;
    private final SoundQuery query;
    private final IOnPostExecute listener;

    public SendCommandTask(SoundApi api, SoundQuery query, IOnPostExecute listener) {
        this.mService = new WeakReference<>(api);
        this.query = query;
        this.listener = listener;
    }

    @Override
    protected Response<Void> doInBackground(SoundQuery... soundQueries) {
        try {
            return mService.get().playSound(this.query).execute();
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    protected void onPostExecute(Response<Void> response) {
        listener.onCommandSent(this.query, response);
    }

    public interface IOnPostExecute {
        void onCommandSent(SoundQuery query, Response<Void> response);
    }
}