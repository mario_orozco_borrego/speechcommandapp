package speech.com.myapplication

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * Created by mario on 16.05.18.
 */

data class SoundQuery(var SoundID: Int = 0)

interface SoundApi {

    @POST("/sound/play")
    fun playSound(@Body body: SoundQuery): Call<Void>
}

